#ifndef CODE_H
#define CODE_H
#include <iostream>
#include <fstream>
#include <cstdlib> //stdlib.h
#include <vector>
class Code{
    std::string text;
    std::vector<char> buf;
    int vectLen;
    int CurrentChNum;
    int NextStrNum;
    int CurrentStrNum;
public:
    Code(std::string FileName);
    char GiveCh();
    char ShowCh();
    int getStrNum();
    bool IsEnd();
};
#endif // CODE_H
