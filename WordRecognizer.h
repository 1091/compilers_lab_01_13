#ifndef WORDRECOGNIZER_H
#define WORDRECOGNIZER_H
#include <string>
#include <fstream>
#include <set>
#include "Code.h"
#include <list>

#define DIGIT_ASCII_LOWER_LIMIT 48
#define DIGIT_ASCII_UPPER_LIMIT 57
#define LETTER_UPPERCASE_ASCII_LOWER_LIMIT 65
#define LETTER_UPPERCASE_ASCII_UPPER_LIMIT 90
#define LETTER_LOWERCASE_ASCII_LOWER_LIMIT 97
#define LETTER_LOWERCASE_ASCII_UPPER_LIMIT 122

#define NUMBER_OF_LETTERS 26
#define NUMBER_OF_DIGITS 10
#define NUMBER_OF_OTHER 4

#define CLASS_TAG_NAMES 1
#define CLASS_TAG_CLOSING 2
#define CLASS_IDENTIFICATOR 5
#define CLASS_BRACKET 3
#define CLASS_ERROR 0
#define MAYBE_CLOSING_TAG 666
#define MAYBE_NOT_CLOSING 667
#define MAGIC_NUMBER 10

#define ERROR_INDEX -1

class WordRecognizer{
    std::string Token;
    std::set<char> availableCh;
    int idCounter;
    Code *code;
    struct state{
        int Class;
        int subClass;
        state *st[2*NUMBER_OF_LETTERS+NUMBER_OF_DIGITS+NUMBER_OF_OTHER]; 
        state(){
            int number = 2*NUMBER_OF_LETTERS+NUMBER_OF_DIGITS+NUMBER_OF_OTHER;
            Class = CLASS_ERROR;
            subClass = CLASS_ERROR;
            for(int i = 0; i < number; i++){
                st[i] = NULL;
			}
        }
    };
    state *startState;
    int CharToInd(char);
    void addReserved(std::string, int, int);
    bool takeWord();
public:
    WordRecognizer(std::string,Code*);
    void getClass(std::list<std::pair<std::pair<int,int>, std::string>> *);
    std::string getToken();
};

#endif // WORDRECOGNIZER_H

