#ifndef CONSTRECOGNIZER_H
#define CONSTRECOGNIZER_H
#include "Code.h"
#include <set>

#define DIGIT_ASCII_LOWER_LIMIT 48
#define DIGIT_ASCII_UPPER_LIMIT 57
#define LETTER_UPPERCASE_ASCII_LOWER_LIMIT 65
#define LETTER_UPPERCASE_ASCII_UPPER_LIMIT 90
#define LETTER_LOWERCASE_ASCII_LOWER_LIMIT 97
#define LETTER_LOWERCASE_ASCII_UPPER_LIMIT 122
#define LETTER_A_UPPERCASE_ASCII 65
#define LETTER_F_UPPERCASE_ASCII 70
#define LETTER_A_LOWERCASE_ASCII 97
#define LETTER_F_LOWERCASE_ASCII 102

#define CLASS_ERROR 0
#define CLASS_INTEGER 6
#define CLASS_COLOR 7
#define	CLASS_STRING 8

#define COLOR_LENGTH 7

class ConstRecognizer{
    Code *code;
    std::set<char> availableAfterStr;
    std::set<char> availableAfterNum;
    std::string Token;
public:
    ConstRecognizer(Code *);
    int getClass();
    std::string getToken();
private:
    int String();
    int Integer(char);
    int Color();
    int errorAnalyser(int,char);    
    enum tableState{
        S_LEX,
        S_END
    };
    enum inputCharacter{
        C_DIGIT
    };    
    int lexTable[1][2] = {
		{S_LEX, S_END}
    };
};

#endif // CONSTRECOGNIZER_H
